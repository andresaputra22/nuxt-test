import type { RouterConfig } from "@nuxt/schema";
// https://router.vuejs.org/api/interfaces/routeroptions.html
export default <RouterConfig>{
  routes: (_routes) => [
    /* STORE FRONT */
    {
      name: "storeFront",
      path: "/",
      component: () => import("~/pages/StoreFront.vue"),
    },
    {
      name: "productDetails",
      path: "/product/:id",
      component: () => import("~/pages/ProductDetails.vue"),
    },
    {
      name: "cart",
      path: "/cart",
      component: () => import("~/pages/StoreCart.vue"),
    },

    /* CHECKOUT LINK */
    {
      name: "checkoutLink",
      path: "/:checkouturl",
      component: () => import("~/pages/CheckoutLink.vue"),
    },
  ],
};
