/**
 * @description 100 = Produk Fisik 200 = Non-Fisik(Jasa)
 */
type productType = 100 | 200;
export interface Variant {
  actualPrice: number;
  id: string;
  qty: number;
  totalPrice: number;
  variantCOGS: number | null;
  variantCategory: Array<{ name: string; content: string }>;
  variantDesc: string;
  variantDiscount: number | null;
  variantHeight: number | null;
  variantIsActive: boolean;
  variantIsMain: boolean;
  variantLength: number | null;
  variantName: string;
  variantPrice: number;
  variantSKU: string;
  variantStock: number | null;
  variantWeight: number;
  variantWidth: number | null;
}
export interface Product {
checkoutUrl: any;
  id: string;
  productCategory: string | string[];
  productCategoryId: number;
  productCategorySlug: string;
  productDescription: string;
  productDiscountEnd: string | null;
  productDiscountStart: string | null;
  productImage: string[];
  productIsActive: boolean;
  productName: string;
  productPriceCogs: number | null;
  productPriceDiscounted: number;
  productPriceUnit: number | null;
  productPriceWholesale: object[];
  productStatus: string;
  productStatusId: number;
  productType: string;
  productTypeId: productType;
  productVariant: Array<Variant>;
  productVideo: string;
  slug: string;
}
