/**
 * @function
 * @returns domain name
 */
export const useDomainName = () => {
  const nuxtApp = useNuxtApp();
  if (process.server) {
    return nuxtApp.ssrContext?.event.node.req.headers.host;
  } else {
    return window.location.host;
  }
};
