import { defineStore } from "pinia";
import type { Product, Variant } from "~~/utils/interfaces/product";

export const useStoreCart = defineStore(
  "storeCart",
  () => {
    const cart = ref<Product[]>([]);
    const errors = ref<string[]>([]);

    const getTotalItems = computed<number>(() =>
      cart.value
        .flatMap((product) => product.productVariant)
        .map((variant) => variant.qty)
        .reduce((prev, curr) => prev + curr, 0)
    );
    const getProductVariant = computed<Variant[]>( () => cart.value.flatMap((product) => product.productVariant))
    const produkJasa = computed(() =>
      cart.value.filter((product) => product.productType === "Non-Fisik")
    );
    const produkFisik = computed(() =>
      cart.value.filter((product) => product.productType === "Fisik")
    );

    /**
     * Handle Add To Cart For Store Front
     * @function
     * @param {Product} payload
     */
    const addToCart = (payload: Product) => {
      const isProductExist: Product[] = cart.value.filter(
        (product: Product) => product.id === payload.id
      );
      const isVariantExist: Variant[] =
        isProductExist[0]?.productVariant.filter(
          (v) => v.variantName === payload.productVariant[0].variantName
        );
      const indexProduct = cart.value
        .map((product: Product) => product.id)
        .indexOf(payload.id);
      const indexVariant = cart.value
        .flatMap((product: Product) => product.productVariant)
        .flatMap((variant) => variant.variantName)
        .indexOf(payload.productVariant[0].variantName);

      // if product not exist add to cart
      if (!isProductExist[0]) {
        cart.value.push(payload);
        return;
      }
      // if product exist check variant
      // if product exist and variant doesnt exist push variant to relatable product
      // else update exisiting variant
      if (!isVariantExist[0]) {
        cart.value[indexProduct].productVariant.push(...payload.productVariant);
      } else {
        cart.value[indexProduct].productVariant[indexVariant].actualPrice =
          payload.productVariant[0].actualPrice;
        cart.value[indexProduct].productVariant[indexVariant].totalPrice =
          payload.productVariant[0].actualPrice * payload.productVariant[0].qty;
        cart.value[indexProduct].productVariant[indexVariant].qty +=
          payload.productVariant[0].qty;
        // isVariantExist[0].actualPrice = payload.productVariant[0].actualPrice;
        // isVariantExist[0].totalPrice += payload.productVariant[0].actualPrice * payload.productVariant[0].qty;
        // isVariantExist[0].qty += payload.productVariant[0].qty;
      }
    };

    /**
     * @function
     * @param payload - product want tobe updated
     */
    const updateCart = (payload: Product) => {
      const indexProduct = cart.value.indexOf(payload.id);
      const indexVariant = cart.value[indexProduct].productVariant
        .flatMap((variant) => variant.variantName)
        .indexOf(payload.productVariant[0].variantName);

      if (!indexProduct) {
        errors.value.push("Invalid Product");
        return;
      }

      cart.value[indexProduct].productVariant[indexVariant] =
        payload.productVariant[0].actualPrice;
      cart.value[indexProduct].productVariant[indexVariant] =
        payload.productVariant[0].actualPrice * payload.productVariant[0].qty;
      cart.value[indexProduct].productVariant[indexVariant] =
        payload.productVariant[0].qty;
    };

    return {
      cart,
      getTotalItems,
      getProductVariant,
      addToCart,
    };
  },
  {
    persist: {
      key: "storeCart",
      storage: persistedState.localStorage,
    },
  }
);
