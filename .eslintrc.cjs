module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  extends: [
    "@nuxtjs/eslint-config-typescript",
    "plugin:prettier/recommended",
    // 'airbnb-base',
  ],
  plugins: [],
  rules: {
    "prettier/prettier": [
      "error",
      {
        endOfLine: "auto",
      },
    ],
    "linebreak-style": 0,
    "consistent-return": 0,
    "import/no-unresolved": 0,
    "no-plusplus": 0,
    "no-underscore-dangle": 0,
    "no-extra-boolean-cast": 0,
    "class-methods-use-this": 0,
    "no-unused-vars": 1,
    "vue/require-default-prop": 0,
    "vue/multi-word-component-names": 0,
    "vuejs-accessibility/form-control-has-label": 0,
    "vue/html-self-closing": 0,
    "vue/max-len": 0,
  },
};
