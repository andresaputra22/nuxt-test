function formatHargaShort(harga: Number) {
  if (harga >= 1000000000) {
    return 'Rp ' + (harga / 1000000000).toFixed(1).replace(/\.0$/, '') + 'M';
  } else if (harga >= 1000000) {
    return 'Rp ' + (harga / 1000000).toFixed(1).replace(/\.0$/, '') + 'jt';
  } else if (harga >= 1000) {
    return 'Rp ' + (harga / 1000).toFixed(1).replace(/\.0$/, '') + 'rb';
  } else {
    return 'Rp ' + harga.toString();
  }
}
/**
 *
 * @param { number } value
 * @returns Formated IDR Number
 */
export const FormatIDR = (value: number, isShort: Boolean) => {
  if (isShort && value > 1000000) {
    return formatHargaShort(value);
  }
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 0,
  }).format(value);
};
